
 /** .well-known/browserid **/

The first point of contact an external site will have with an IDP is the browserid file. It’s always storted off the site root in a directory called ‘.well-known’, and contains a simple json object, which either delegates authority to another IDP, or specifies the public key for this IDP, and its endpoints.



{
  "authority": "pds-stage01.mixcic.eu"
}

Example of a .browserid file delegating authority.

{
    "authentication": "/browserid/sign_in",
    "provisioning": "/browserid/provision",
    "public-key": {"algorithm":"RS","n":"11736938104519915029331498772761826038068079456660400652167606529551552579170441089503540592061628940217583879830373688054791688543844170306782262231598337","e":"65537"}
}

Example of a .browserid file providing endpoints and 64 bit RSA key.

 /** Drupal module **/

The module’s job is to provide the appropriate javascript at both of the endpoints expected by Persona, and to integrate these endpoints with Drupal’s authentication and session handling subsystems.

In most cases on our recommended platform, it’s enough to enable the module and leave it. If, however, we have a certifier running on a separate port or IP, it can be changed by setting the ‘browserid_nodejs_certifier’ $conf variable within Drupal’s settings.php. It should contain a full url at which a certifier can be found (e.g. http://localhost:8181/cert_key)

 /** Certifier **/

This is a node.js application which is run and served locally, usually on port 8181. I’m not sure of the internals of it all, since I don’t know node, but it’s there to take a user’s details, and produce a signed certificate to return to a browserid enabled application, which shows that the user has a valid session at the IDP.

To install, this application uses the npm utility to manage all its prerequisites. Clone the app from git@github.com:mozilla/browserid-certifier.git (we check out the commit at “6891be3bf8787e0ce5db6039d2f9a1e97becfb13” - dated around April 15th 2013. This is to keep the app compatible with our local version of node.js).

On our servers, the code is checked out to /usr/local/src/browserid-certifier/

Once checked out, the application needs to have its prerequisites installed via npm. This is a matter of running:

sudo npm install

from the root of the app.

Once we have the application installed and set up, we need to add a config file, and generate a certificate for the application to use to sign certificates. We can do this by running:

mkdir var
cd var/
../node_modules/.bin/generate-keypair
the generate-keypair command takes different parameters. I’ve found that the keys that work best with browserid-certifier are 64 bit RSA keys.

Here’s a sample config file - it’s just json, and lives under the /config subdirectory:

{
  "ip": "127.0.0.1",
  "port": 8181,
  "issuer_hostname": "my.domain.com",
  "pub_key_path": "var/key.publickey",
  "priv_key_path": "var/key.secretkey"
}


By default, the file ‘local.json’ will be loaded when the app runs. Running the node application is a matter of running:

sudo node bin/certifier

It can also be run via:

npm start

from the root of the certifier application. However, this doesn’t necessarily run the application in the background, and is dependent on the user’s shell, so will terminate when the user logs out. Hence we require an init.d script to run the certifier as a service.

You can override the config file you pass to the certifier by specifying it as an environment variable - e.g.:
CONFIG_FILES=config/local.json npm start


 /** init.d service **/

This is dependent on the version and type of linux you’re running, but basically you’re going to want a script to run the node app as a daemon. The script I sourced and edited to achieve this can be found at: https://gist.github.com/instanceofjamie/403b08f6f73b0fc9fc32 feel free to copy and edit this.
